const btn= document.querySelector(".btn");

btn.addEventListener("click", () => {
    var inputelement = document.querySelector("#textoo")
    var input = inputelement.value
    // const textoo = document.querySelector("#textoo")
    const middle = document.querySelector(".middle")
    const conversation = document.createElement("div")
    conversation.classList.add(".create")

    const paragrafo= document.createElement("textarea")
    paragrafo.classList.add("reply")
    paragrafo.disabled=true
    paragrafo.textContent=input
    conversation.appendChild(paragrafo)


    const mensagens= document.createElement("div")
    mensagens.classList.add("mensagens")

    var editar= document.createElement("button")
    editar.classList.add("editar")
    editar.innerText="Editar"
    mensagens.appendChild(editar)

    var excluir= document.createElement("button")
    excluir.classList.add("excluir")
    excluir.innerText="Excluir"
    mensagens.appendChild(excluir)

    conversation.appendChild(mensagens)
    var counter=1
    inputelement.value=""
    

    middle.appendChild(conversation)
    excluir.addEventListener("click", (e) => {
        e.currentTarget.parentElement.parentElement.remove()
        })
    editar.addEventListener("click", (e) => {
        if(counter % 2==0){
            paragrafo.disabled=true
            counter+=1
            editar.innerText="Editar"
            paragrafo.style.backgroundColor= "transparent"
            editar.style.backgroundColor="#F6A75D"
            
        }
        else if(counter % 2!=0){
            paragrafo.disabled=false
            counter+=1
            editar.innerText="Finalizar"
            editar.style.backgroundColor="#22e636"
            paragrafo.style.backgroundColor= "rgb(188, 206, 209, 0.4)"
        }
        

        })
})
